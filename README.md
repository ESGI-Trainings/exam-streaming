# Développer une application de streaming
 
## Exercice 1 (4 pt):
 
Un contenu possède:

- Un titre 
- Une durée
 

Un contenu peut être : 

- En une seule partie (ex : un film) 
- En plusieurs épisodes (ex : un documentaire) 
- En plusieurs saisons de plusieurs épisodes (ex : une série)

 Ecrire les différentes classes/interfaces/etc. pour gérer le contenu
 
### Ecrire un tests qui valide que la durée d'un film (1 pt) 
### Ecrire un tests validant la durée d'un documentaire en plusieurs parties (1 pt)
###  Ecrire un tests validant la durée d'une série en plusieurs saisons (2 pt)
 

##  Exercice 2 (5 pt) :
 
Un contenu a une classification (jeune, tout public ou public averti).  Un contenu est dans au moins une des catégories suivantes :

- documentaire
- comédie
- drama
- science-fiction
- aventure
- horreur
 

Un utilisateur possède : 

- un nom 
- un historique du contenu regardé
 

Un utilisateur peut :

 - Visionner un contenu (il est alors ajouté à son historique.)
 - Afficher l'historique des vidéos qu'il a regardé
 - Afficher sa catégorie préférée (i.e. la catégorie dont il a le plus regardé de contenu) 
- Enregistrer un contenur comme "à regarder plus tard". Le contenu sera retiré de cette liste une fois visionné


 ### Ecrire la classe User
 
### Ecrire un tests validant que l'utilisateur peut afficher son historique (1 pt)
### Ecrire un tests validant que l'utilisateur peut connaître sa catégorie préféré (1 pt)
### Ecrire un tests validant que l'utilisateur peut sauvegarder un contenu pour plus tard (1 pt)
### Ecrire un tests validant que le contenu enregistré pour plus tard est retiré de la liste une fois lu (2 pt)
 
 ## Exercice 3 (4 pt)
 
Un utilisateur peut voir tous les contenus sauf si, lors dans sa création, il est indiqué comme "profil enfant".
Il ne pourra alors voir que les contenus pour enfant ou tout public.
 
Modifier la classe User pour ajouter le ou les attributs manquants 
Modifier la méthode permettant de visionner un contenu pour retourner un booléen (vrai l'utilisateur peut lire le contenu)
 
### Ecrire un tests validant qu'un utilisateur classique peut accéder à tout le contenu (1 pt) 
### Ecrire un tests validant qu'un utilisateur enfant peut accéder au contenu pour enfant ou tout public (1 pt)
### Ecrire un tests validant qu'un utilisateur enfant ne peut pas accéder au contenu pour public averti (2 pt)
 
## Exercice 4 (7 pt)
 
L'historique d'un utilisateur doit afficher le jour où un contenu a été visionné.
 
### Créer une classe permettant de gérer l'historique (date + contenu) (1 pt) 

### Modifier la classe User pour sauvegarder le nouvel historique (3 pt)

### Ecrire un tests permettant de retrouver une information dans l'historique (5pt)

 A partir du flot suivant : 
1. Les Oiseaux (12 novembre 2020) 
2. Psychose (20 Novembre 2020)
3. Fenêtre sur cour (2 Décembre 2020) 
4. Sueurs froides (3 Décembre 2020) 
5. La Mort aux trousses (15 Décembre 2020) 
6. Psychose (5 Janvier 2020) 

Ecrire un tests validant que l'utilisateur a bien regardé "Fenêtre sur cour" le 2 Décembre 2020
